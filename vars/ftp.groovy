// See https://github.com/jenkinsci/workflow-cps-global-lib-plugin
/// Example code to call ftp function
//ftp{
//host = ""
//username = "user1"
//password = "pass1"
//remote_diectory = ""
//local_directory = ""
//local_directory_replace = ""
//}


def call(config) {

env.ftpUploadScript="""
import os
from ftplib import FTP
def chdir(ftp_path, ftp_conn):
	dirs = [d for d in ftp_path.split('/') if d != '']
	for p in dirs:
		check_dir(p, ftp_conn)
def check_dir(dir, ftp_conn):
	filelist = []
	ftp_conn.retrlines('LIST', filelist.append)
	found = False
	for f in filelist:
		if f.split()[-1] == dir and f.lower().startswith('d'):
			found = True
	if not found:
		ftp_conn.mkd(dir)
	ftp_conn.cwd(dir)
def upload_file(ftp, file):
	fileName = file.rsplit('/', 1)[1]
	fileData = open(file,'r')
	ftp.storbinary('STOR '+fileName, fileData)
	fileData.close()
session = FTP('"""+config.host+"""','"""+config.username+"""','"""+config.password+"""',timeout=20)
FTP.set_debuglevel(session,2)
session.cwd('"""+config.remote_diectory+"""')
local_file_directory = '"""+config.local_directory+"""'
directory_depp_length = 0
for dirName, subdirList, fileList in os.walk(local_file_directory, topdown=False):
	currentDir = dirName.replace('"""+config.local_directory_replace+"""','')
	if not currentDir.startswith('.svn') and not currentDir.startswith('.git'):
		chdir(currentDir,session)
		for fname in fileList:
			upload_file(session,dirName+'/'+fname)
		directory_depp_length = len(currentDir.split('/'))
		for i in range(0,directory_depp_length):
			session.cwd('..')
session.quit()
"""

env.ftpUploadScript = env.ftpUploadScript.replace("\n", "^")
env.ftpUploadScript = env.ftpUploadScript.replace("\t", "")

/*
echo "-------------------------------------------------------------------"
echo env.ftpUploadScript
echo "-------------------------------------------------------------------"


bat """ 
echo ${env.ftpUploadScript}
"""
*/

bat """
python -c "${env.ftpUploadScript}"
"""
/*
sh '''
set +x
python -c "$ftpUploadScript"
'''*/
}